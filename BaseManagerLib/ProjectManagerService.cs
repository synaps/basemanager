﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;

namespace BaseManagerLib
{
    public class ProjectManagerService
    {
        void CreateDB(string pLocation)
        { }
        void CreateUserFile(string pLocation)
        { }

        //ProjectFiles
        public static void CreateProjectXml(string pFileName, Project pProject)
        {
            XmlDocument Document_Project = new XmlDocument();
            XmlElement RootNode;
            XmlElement ElementWithText;

            RootNode = Document_Project.CreateElement("project");
            ElementWithText = Document_Project.CreateElement("database");
            XmlElement Element = Document_Project.CreateElement("host");
            Element.AppendChild(Document_Project.CreateTextNode(pProject.DatabaseHost)); //Database host location
            ElementWithText.AppendChild(Element);
            Element = Document_Project.CreateElement("base");
            Element.AppendChild(Document_Project.CreateTextNode(pProject.Database));
            ElementWithText.AppendChild(Element);
            Element = Document_Project.CreateElement("user");
            Element.AppendChild(Document_Project.CreateTextNode(pProject.DatabaseUser));
            ElementWithText.AppendChild(Element);
            Element = Document_Project.CreateElement("password");
            Element.AppendChild(Document_Project.CreateTextNode(pProject.DatabasePassword));
            ElementWithText.AppendChild(Element);
            RootNode.AppendChild(ElementWithText);
            
            //XmlElement Projects = Document_Project.CreateElement("projects");

            //for (int i = 0; i < pProject.Projects.Count; i++)
            //{
            //    ElementWithText = Document_Project.CreateElement("project");
            //    ElementWithText.AppendChild(Document_Project.CreateTextNode(pProject.Projects[i]));
            //    Projects.AppendChild(ElementWithText);
            //}
            //RootNode.AppendChild(Projects);

            Document_Project.AppendChild(RootNode);
            Document_Project.Save(pFileName);
        }
        public static string DownloadFile(string pFileName, NetworkCredential pCredentials)
        {
            Stream DownloadStream = new MemoryStream(), WriteStream = new MemoryStream();
            string FileName = "";

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(pFileName);
                request.Credentials = pCredentials;
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                DownloadStream = response.GetResponseStream();

                byte[] Data = new byte[2048];
                FileName = "project" + DateTime.Now.Day;
                WriteStream = File.OpenWrite(FileName);

                int size = 0;
                while ((size = DownloadStream.Read(Data, 0, 2048)) > 0)
                    WriteStream.Write(Data, 0, size);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                DownloadStream.Close();
                WriteStream.Close();
            }
            return FileName;
        }
        public static void LoadProjectXml(string pFileName, out Project pProject)
        {
            string Host = "", User = "", Password = "", Base = "";
            try
            {
                XmlDocument Document_Project = new XmlDocument();             
                Document_Project.Load(pFileName);

                XmlNode Element = Document_Project.FirstChild.FirstChild;
                XmlNode TextNode = Element.SelectSingleNode("host");
                Host = TextNode.InnerText;
                TextNode = Element.SelectSingleNode("user");
                User = TextNode.InnerText;
                TextNode = Element.SelectSingleNode("password");
                Password = TextNode.InnerText;
                TextNode = Element.SelectSingleNode("base");
                Base = TextNode.InnerText;

                pProject = new Project(Host, Base, User, Password);
            }
            catch (Exception)
            {
                pProject = new Project(Host, Base, User, Password);
            }
        }
    }
}
