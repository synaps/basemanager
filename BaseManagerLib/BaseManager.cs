﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using MySql.Data.MySqlClient;

namespace BaseManagerLib
{
    public enum SQLOperation : byte
    {
        Create = 0,
        Use,
        Insert,
        Select,
        Delete,
        Drop,
        Decs,
        Update,
        AlterTable
    }

    public class BaseManager
    {
        MySqlConnection Connection;
        string ConnectionString;

        public BaseManager(Project pProject)
        {
            //building connection string
            ConnectionString = "Uid = " + pProject.DatabaseUser + ";";
            ConnectionString += "Pwd = " + pProject.DatabasePassword + ";";
            ConnectionString += "Server = " + pProject.DatabaseHost + ";";
            ConnectionString += "Database = " + pProject.Database + ";";

        }

        private void Connect()
        {
            Connection = new MySqlConnection(ConnectionString);
            Connection.Open();
        }
        private void Disconnet()
        {
            Connection.Close();
        }
        public void ExplicitlyDisconnect()
        {
            Connection.Close();
        }

        public bool CheckConnection()
        {
            try
            {
                Connect();
                Disconnet();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /*public void CreateCommand(SQLOperation pType)
        {
            if (pType == SQLOperation.Create)
            {
            }
            else if (pType == SQLOperation.Select)
            {
            }
            else if (pType == SQLOperation.Delete)
            {
            }
        }
        */

        public DataSet ExecuteQuery(string pCommand)
        {
            Connect();
            MySqlDataAdapter Adapter = new MySqlDataAdapter(pCommand, Connection);
            DataSet DataSet0 = new DataSet();

            try
            {
                Adapter.Fill(DataSet0);
            }
            catch
            {
            }
            finally
            {
                Disconnet();
            }

            return DataSet0;
        }
        public void ExecuteNonQuery(string pCommand)
        {
            Connect();
            MySqlCommand Command = new MySqlCommand(pCommand, Connection);

            try
            {
                int result = Command.ExecuteNonQuery();
            }
            catch
            {
                return;
            }
            finally
            {
                Disconnet();
            }
        }
    }
}
