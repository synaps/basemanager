﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace BaseManagerLib
{
    public static class Invoice
    {
        public static void CreateInvoice(string pLogoImage, string pFooterImage, string pInvoiceName, List<string> pListOfServices)
        {
            Document PDF = new Document();
            PdfWriter.GetInstance(PDF, new FileStream(Environment.CurrentDirectory + "/Invoices/" + pInvoiceName + ".pdf", FileMode.Create));

            PdfPTable PDFTable = new PdfPTable(3);
            PDFTable.DefaultCell.Border = 0;

            Image Logo = Image.GetInstance(pLogoImage);
            Logo.ScaleToFit(PDF.PageSize.Width - 72, Logo.Height);

            PdfPCell cell = new PdfPCell(new Phrase("Arve maksaja:"));
            cell.Colspan = 2;
            cell.Border = 0;

            PDFTable.AddCell(cell);
            PDFTable.AddCell("Arve №:");

            cell.Phrase = new Phrase("#maksaja");
            PDFTable.AddCell(cell);
            PDFTable.AddCell("#arv. number");

            cell.Phrase = new Phrase("Reg. nr.:");
            PDFTable.AddCell(cell);
            PDFTable.AddCell("Kuupaev:");

            cell.Phrase = new Phrase("#reg.num");
            PDFTable.AddCell(cell);
            PDFTable.AddCell("#kuupaev");

            cell.Phrase = new Phrase("Aadress:");
            PDFTable.AddCell(cell);
            PDFTable.AddCell("Tasuda kuni");

            cell.Phrase = new Phrase("#aadress:");
            PDFTable.AddCell(cell);
            PDFTable.AddCell("#tasuda kuni");

            cell.Phrase = new Phrase("Contact andmed:");
            PDFTable.AddCell(cell);
            PDFTable.AddCell("Viivis");

            cell.Phrase = new Phrase("#contact andmed");
            PDFTable.AddCell(cell);
            PDFTable.AddCell("#Viivis");

            PdfPCell TableCell = new PdfPCell();
            TableCell.Colspan = 3;
            TableCell.Border = 0;

            PdfPTable InnerTable = new PdfPTable(6);

            InnerTable.DefaultCell.Border = 0;

            float[] widths = new float[6] { 1f, 10f, 3f, 2f, 2f, 3f };
            InnerTable.SetWidths(widths);

            InnerTable.AddCell("№");
            InnerTable.AddCell("Teenuse nimetus");
            InnerTable.AddCell("Suurus");
            InnerTable.AddCell("Hind");
            InnerTable.AddCell("-%");
            InnerTable.AddCell("Summa");

            foreach (var item in pListOfServices)
            {
                InnerTable.AddCell((pListOfServices.IndexOf(item) + 1).ToString());
                InnerTable.AddCell("$reklaam");
                InnerTable.AddCell("$44x155");
                InnerTable.AddCell("$ 100");
                InnerTable.AddCell("5%");
                InnerTable.AddCell("$ 95");
            }

            TableCell.Table = InnerTable;
            PDFTable.AddCell(TableCell);

            PdfPTable InnerTableSum = new PdfPTable(2);

            InnerTableSum.DefaultCell.Border = 0;

            widths = new float[2] { 8f, 2f };
            InnerTableSum.SetWidths(widths);

            PdfPCell ITSCell = new PdfPCell(new Phrase("Kokku"));
            ITSCell.HorizontalAlignment = 2;
            ITSCell.Border = 0;

            InnerTableSum.AddCell(ITSCell);
            InnerTableSum.AddCell("#sum");

            ITSCell.Phrase = new Phrase("Kaibemaks");
            InnerTableSum.AddCell(ITSCell);
            InnerTableSum.AddCell("#kaibemaks");

            ITSCell.Phrase = new Phrase("Tasuda");
            InnerTableSum.AddCell(ITSCell);
            InnerTableSum.AddCell("#tasuda");

            TableCell.Table = InnerTableSum;
            PDFTable.AddCell(TableCell);

            Image Footer = Image.GetInstance(pFooterImage);
            Footer.ScaleToFit(PDF.PageSize.Width - 72, Footer.Height);

            PdfPTable UnderFooter = new PdfPTable(1);
            UnderFooter.DefaultCell.HorizontalAlignment = 1;
            UnderFooter.DefaultCell.Border = 0;

            UnderFooter.AddCell("#swedbank...");
            UnderFooter.AddCell("#firm");
            UnderFooter.AddCell("#reg.num");
            UnderFooter.AddCell("#kmkr");
            UnderFooter.AddCell("#address");


            PDF.Open();

            PDF.Add(Logo);
            PDF.Add(PDFTable);
            PDF.Add(Footer);
            PDF.Add(UnderFooter);

            PDF.Close();
        }
        public static string CreateDocumentNumber(DateTime pDate)
        {
            return "";
        }
    }
}
