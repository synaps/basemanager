﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace BaseManagerLib
{
    public class Project
    {
        public string DatabaseHost, Database;
        public string DatabaseUser, DatabasePassword;
        public ObservableCollection<InternalProject> Projects;

        public Project()
        {
            Projects = new ObservableCollection<InternalProject>();
        }
        public Project(string pDBHost, string pDatabase, string pDatabaseUser, string pDatabasePassword)
        {
            DatabaseHost = pDBHost;
            Database = pDatabase;
            DatabaseUser = pDatabaseUser;
            DatabasePassword = pDatabasePassword;

            Projects = new ObservableCollection<InternalProject>();
        }
        public Project(string pDBHost, string pDatabase, string pDatabaseUser, string pDatabasePassword, ObservableCollection<string> pProjects)
        {
            DatabaseHost = pDBHost;
            Database = pDatabase;
            DatabaseUser = pDatabaseUser;
            DatabasePassword = pDatabasePassword;
            Projects = new ObservableCollection<InternalProject>();
            foreach (string item in pProjects)
            {
                Projects.Add(new InternalProject(item));
            }
        }

        public void AddProject(InternalProject pProject)
        {
            if (!Projects.Any(x => x.ProjectName == pProject.ProjectName))
                Projects.Add(pProject);
        }
        public void RemoveProject(InternalProject pProject)
        {
            if(Projects.Any(x => x.ProjectName == pProject.ProjectName))
                Projects.Remove(pProject);
        }
    }
    public class InternalProject
    {
        //Data section
        public string ProjectName
        {
            get;
            set;
        }
        public List<User> Users
        {
            get;
            set;
        }

        //Constructors
        public InternalProject(string pProjectName)
        {
            ProjectName = pProjectName;
            Users = new List<User>();
        }

        //Functions
        public void AddUser(User pUser)
        {
            if (!Users.Any(x => x.PersonalCode == pUser.PersonalCode))
                Users.Add(pUser);
        }
        public void RemoveUser(User pUser)
        {
            if (Users.Any(x => x.PersonalCode == pUser.PersonalCode))
                Users.Remove(pUser);
        }
    }
}
