﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.ObjectModel;

namespace BaseManagerLib
{
    [Serializable]
    public class Users //: INotifyCollectionChanged
    {
        //public event NotifyCollectionChangedEventHandler CollectionChanged;
        //public void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        //{
        //    if (CollectionChanged != null)
        //        CollectionChanged(this, e);
        //}

        private ICollection<User> userList;
        public ICollection<User> UserList
        {
            get
            {
                return userList;
            }
            set
            {
                userList = value;
                //OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        public bool AddUser(User pUser)
        {
            if(!UserList.Contains(pUser, new Compare()))
            {
                UserList.Add(pUser);

                //OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, pUser));
                return true;
            }
            else
            {
                return false;
            }
        }
        public void RemoveUser(User pUser)
        {
            if (UserList.Contains(pUser, new Compare()))
            {
                UserList.Remove(pUser);
                //OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, pUser));
            }
        }
        public void Clear()
        {
            userList.Clear();
        }

        public Users()
        {
            UserList = new ObservableCollection<User>();
        }
        /*
        public static void SaveFile(string pFileName, Users pObject)
        {
            IFormatter Serializer = new BinaryFormatter();
            using(FileStream SaveFile = new FileStream(pFileName, FileMode.Create, FileAccess.Write))
            {
                Serializer.Serialize(SaveFile, pObject);
            }
        }
        public static void LoadFile(string pFileName, Users pObject)
        {
            IFormatter Serializer = new BinaryFormatter();
            using (FileStream LoadFile = new FileStream(pFileName, FileMode.Open, FileAccess.Read))
            {
                pObject = Serializer.Deserialize(LoadFile) as Users;
            }
        }
        */

        public User FindUser(string pCriteria, string pData)
        {
            switch (pCriteria)
            {
                case "FullName":
                    return UserList.Single(x => x.FirstName == pData);
                case "PersonalCode":
                    return UserList.Single(x => x.PersonalCode == pData);
                case "Nickname":
                    return UserList.Single(x => x.Nickname == pData);
            }
            return null;
        }
        class Compare : IEqualityComparer<User>
        {
            public bool Equals(User x, User y)
            {
                if (x.PersonalCode == y.PersonalCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            public int GetHashCode(User pUser)
            {
                return 0;
            }
        }
    }

    [Serializable]
    public class User : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        string firstName;
        public string FirstName
        {
            get 
            {
                return firstName;
            }
            set 
            {
                firstName = value;
                OnPropertyChanged(new PropertyChangedEventArgs("FirstName"));
            }
        }
        string personalCode;
        public string PersonalCode
        {
            get
            {
                return personalCode;
            }
            set
            {
                personalCode = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PersonalCode"));

            }
        }
        string nickname;
        public string Nickname
        {
            get
            {
                return nickname;
            }
            set
            {
                nickname = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Nickname"));

            }
        }
        string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                //OnPropertyChanged(new PropertyChangedEventArgs("Password"));
            }
        }
        byte permissions;
        public byte Permissions
        {
            get
            {
                return permissions;
            }
            set
            {
                permissions = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Permissions"));
            }
        }
/*        public ICollection<string> Projects
        {
            get;
            set;
        }
 */

        public User(string pFirstName, string pPersonalCode, string pNickName, string pPassword, byte pPermissionLevel)
        {
            FirstName = pFirstName;
            PersonalCode = pPersonalCode;
            Nickname = pNickName;
            Password = pPassword;
            Permissions = pPermissionLevel;
        }

        /*public void AddProject(string pProject)
        {
            if (!Projects.Contains(pProject))
                Projects.Add(pProject);
        }*/
    }
}