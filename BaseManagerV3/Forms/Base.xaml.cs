﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.Entity;

namespace BaseManager.Forms
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Base : Window
    {
        ObservableCollection<Data> ProjectData;
        List<Data> NewData;
        Data Entry, CurrentItem;
        bool CanAddRow = true;

        public Base()
        {
            InitializeComponent();

            NewData = new List<Data>();

            //window managment logic
            this.Hide();
            Login LoginForm = new Login();
            if (LoginForm.ShowDialog() == true)
            {
                ProjectManager ProjectManagerForm = new ProjectManager();
                ProjectManagerForm.ShowDialog();
                if (ProjectManagerForm.DialogResult.HasValue && ProjectManagerForm.DialogResult.Value)
                {
                    this.Show();
                }
                else
                    Environment.Exit(0);
            }
            else
            {
                Environment.Exit(0);
            }
        }

        //Starting and stoping window
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            App.Model = new DatabaseModel(App.CreateConnectionString("infoleht.ee", "infoleht_base", "infoleht_andmebaas", "Infoleht123"));
            var ProjectData = App.Model.Data.Where(data => data.Project == App.CurrentProject.Name);
            this.ProjectData = new ObservableCollection<Data>(ProjectData.ToList());
            DataGrid_Main.ItemsSource = this.ProjectData;
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (Data item in NewData)
            {
                App.Model.Data.Add(item);
            }

            try
            {
                App.Model.SaveChanges();
                App.Model.Dispose();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                return;
            }
        }

        private void Button_AddRow_Click(object sender, RoutedEventArgs e)
        {
            if (!CanAddRow)
                return;
            CanAddRow = false;
            Entry = new Data() { Id = 0, Project = App.CurrentProject.Name };
            ProjectData.Add(Entry);
            NewData.Add(Entry);
        }
        private void Button_DeleteRow_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid_Main.SelectedItems != null)
            {
                if (DataGrid_Main.SelectedItems.Count > 1)
                    MessageBox.Show("You are going to delete more than one entry");
                var Items = DataGrid_Main.SelectedItems;
                for (int i = 0; i < Items.Count; i++)
                {
                    if (NewData.Contains(Items[i] as Data))
                    {
                        NewData.Remove(Items[i] as Data);
                        ProjectData.Remove(Items[i] as Data);
                    }
                    else
                    {
                        App.Model.Data.Remove(Items[i] as Data);
                        ProjectData.Remove(Items[i] as Data);
                    }
                }
                CanAddRow = true;
            }
        }
        private void Button_Search_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DataGrid_Main_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataGrid_Main.SelectedItem == null)
                return;
            Data EditableEntity = DataGrid_Main.SelectedItem as Data;
            if (NewData.Contains(EditableEntity))
            {
                CurrentItem = NewData[NewData.IndexOf(EditableEntity)];
            }
            else
            {
                foreach (var item in App.Model.Data)
                {
                    if (item == EditableEntity)
                    {
                        CurrentItem = item;
                    }
                }
            }
            //var DBEditableEntity = App.Model.Data.FirstOrDefault(;
        }
        private bool isManualEditCommit;
        private void DataGrid_Main_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (!isManualEditCommit)
            {
                isManualEditCommit = true;
                DataGrid grid = (DataGrid)sender;
                grid.CommitEdit(DataGridEditingUnit.Row, true);
                isManualEditCommit = false;
            }
            Data Item = (Data)DataGrid_Main.SelectedItem;
            if (Item != null)
                if (Item.Firm != null)
                    CanAddRow = true;
        }
        //Edition check
        private void DataGrid_Main_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            Data current = DataGrid_Main.SelectedItem as Data;
            if (CurrentItem.Firm != current.Firm)
            {
                CurrentItem.Firm = current.Firm;
            }
            if (CurrentItem.PhysicalAddress != current.PhysicalAddress)
            {
                CurrentItem.PhysicalAddress = current.PhysicalAddress;
            }
            if (CurrentItem.WebAddress != current.WebAddress)
            {
                CurrentItem.WebAddress = current.WebAddress;
            }
            if (CurrentItem.LastContact != current.LastContact)
            {
                CurrentItem.LastContact = current.LastContact;
            }
            if (CurrentItem.NextContact != current.NextContact)
            {
                CurrentItem.NextContact = current.NextContact;
            }
            if (CurrentItem.Category != current.Category)
            {
                CurrentItem.Category = current.Category;
            }
            if (CurrentItem.ContactEmail != current.ContactEmail)
            {
                CurrentItem.ContactEmail = current.ContactEmail;
            }
            if (CurrentItem.ContactLanguage != current.ContactLanguage)
            {
                CurrentItem.ContactLanguage = current.ContactLanguage;
            }
            if (CurrentItem.ContactPerson != current.ContactPerson)
            {
                CurrentItem.ContactPerson = current.ContactPerson;
            }
            if (CurrentItem.ContactPhone1 != current.ContactPhone1)
            {
                CurrentItem.ContactPhone1 = current.ContactPhone1;
            }
            if (CurrentItem.ContactPhone2 != current.ContactPhone2)
            {
                CurrentItem.ContactPhone2 = current.ContactPhone2;
            }
            if (CurrentItem.Notes != current.Notes)
            {
                CurrentItem.Notes = current.Notes;
            }
        }

        private void Button_Reload_Click(object sender, RoutedEventArgs e)
        {
        }
        private void Button_DiscartChanges_Click(object sender, RoutedEventArgs e)
        {
            App.DisposeModel();
            App.CreateModel();
            var ProjectData = App.Model.Data.Where(data => data.Project == App.CurrentProject.Name);
            this.ProjectData = new ObservableCollection<Data>(ProjectData.ToList());
            DataGrid_Main.ItemsSource = this.ProjectData;

            CanAddRow = true;
        }
        private void Button_SaveChanges_Click(object sender, RoutedEventArgs e)
        {
            List<Data> NotValid = new List<Data>();
            foreach (Data item in NewData)
            {
                if ((item.Firm == null) || (item.ContactPerson == null))
                    NotValid.Add(item);
                else
                    App.Model.Data.Add(item);
            }
            string Message = ""; int i = 0;
            foreach (Data item in NotValid)
            {
                i++;
                Message += string.Format("{0}: Firm: {1}, ContactPerson {2}\n", i, item.Firm, item.ContactPerson);
            }
            MessageBox.Show(Message + "weren't added");
            App.Model.SaveChanges();

            CanAddRow = true;
        }
    }
}
