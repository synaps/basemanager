﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BaseManager.Forms
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class Registration : Window
    {

        public Registration()
        {
            InitializeComponent();
            ListBox_Users.ItemsSource = App.Users;
        }

        private void Button_AddUser_Click(object sender, RoutedEventArgs e)
        {
            switch (ComboBox_Permissions.Text)
            {
                case "Grand Administrator":
                    ComboBox_Permissions.Tag = (sbyte)3;
                    break;
                case "Administrator":
                    ComboBox_Permissions.Tag = (sbyte)2;
                    break;
                case "User":
                    ComboBox_Permissions.Tag = (sbyte)1;
                    break;
            }//EndSwitch
            App.Users.Add(new Users() { FirstName = TextBox_FirstName.Text, LastName = TextBox_LastName.Text, IdCode = TextBox_IDCode.Text, Login = TextBox_Nickname.Text, Password = TextBox_Password.Text, Privileges = (sbyte)ComboBox_Permissions.Tag });
        }
        private void Button_EditUser_Click(object sender, RoutedEventArgs e)
        {
            if (ListBox_Users.SelectedItem == null)
                return;
            BaseManagerLib.User EditableUser = ListBox_Users.SelectedItem as BaseManagerLib.User;
        }
        private void Button_RemoveUser_Click(object sender, RoutedEventArgs e)
        {
            //if (ListBox_Users.SelectedItems.Count != 0)
                //App.Users.RemoveUser((BaseManagerLib.User)ListBox_Users.SelectedItem);
        }

        private void Button_Register_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(App.Users.Count.ToString());
            
            this.DialogResult = true;
            this.Close();            
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }


    }
}