﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BaseManager.Forms
{
    /// <summary>
    /// Interaction logic for ProjectManager.xaml
    /// </summary>
    public partial class ProjectManager : Window
    {
        System.Timers.Timer AutoChoose = new System.Timers.Timer(5000);
        Projects AutoProject;

        public ProjectManager()
        {
            InitializeComponent();
        }

        private void Menu_ManageProjects_Click(object sender, RoutedEventArgs e)
        {
            ProjectManagerWindows.Projects ProjectManager = new ProjectManagerWindows.Projects();
            ProjectManager.ShowDialog();
            if (ProjectManager.DialogResult.HasValue && ProjectManager.DialogResult.Value)
            {
            }
            else
            {
            }
        }
        private void Menu_ManageUsers_Click(object sender, RoutedEventArgs e)
        {
            ProjectManagerWindows.Users UsersManager = new ProjectManagerWindows.Users();
            UsersManager.ShowDialog();
            if (UsersManager.DialogResult.HasValue && UsersManager.DialogResult.Value)
            {
            }
        }
        private void Menu_MyAccount_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Select_Click(object sender, RoutedEventArgs e)
        {

            if (ListView_Projects.SelectedItem == null)
                return;
            if (CheckBox_Save.IsChecked == true)
            {
                App.Settings.RememberedProject = ((Projects)ListView_Projects.SelectedItem).Name;
                App.Settings.RememberProject = true;
                App.Settings.Save();
            }
            else
            {
                App.Settings.RememberProject = false;
                App.Settings.Save();
            }
            App.CurrentProject = ListView_Projects.SelectedItem as BaseManager.Projects;
            this.DialogResult = true;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Menu visibility
            if (App.CurrentUser.Privileges >= 2)
                Menu_Admin.Visibility = System.Windows.Visibility.Visible;
            else
                Menu_User.Visibility = System.Windows.Visibility.Visible;

            //Connecting
            App.CreateModel();

            ListView_Projects.ItemsSource = App.Model.Users.First(user => user.IdCode == App.CurrentUser.IdCode).Projects.ToList();

            //Closing connection
            App.DisposeModel();
            CheckBox_Save.IsChecked = App.Settings.RememberProject;
            if ((App.Settings.RememberedProject != null) && (App.Settings.RememberProject))
            {
                foreach (Projects item in ListView_Projects.Items)
                {
                    if (item.Name == App.Settings.RememberedProject)
                    {
                        AutoProject = item;
                        AutoChoose.Elapsed += AutoChoose_Elapsed;
                        AutoChoose.Start();
                        break;
                    }
                    else
                    {
                        MessageBox.Show("Problems with project selection.");
                    }
                }
            }
        }

        void AutoChoose_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (Dispatcher.CheckAccess())
                {
                    AutoChoose.Stop();
                    App.CurrentProject = AutoProject;
                    if (this.IsActive)
                    {
                        this.DialogResult = true;
                        this.Close();
                    }
                }
                else
                {
                    Action action = delegate()
                     {
                         AutoChoose.Stop();
                         App.CurrentProject = AutoProject;
                         if (this.IsActive)
                         {
                             this.DialogResult = true;
                             this.Close();
                         }
                     };
                    Dispatcher.Invoke(action);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void ListView_Projects_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Button_Select_Click(this, new RoutedEventArgs());
        }
    }
}
