﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.IO;
using System.Net;
using System.Data;
using System.Collections.ObjectModel;

namespace BaseManager.Forms
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        bool LoadedPass = false;
        public Login()
        {
            InitializeComponent();
        }

        private void Button_Login_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                string Host, Database, Login, Password, DBLogin, DBPassword;
                Host = TextBox_Host.Text;
                Database = TextBox_Database.Text;
                Login = TextBox_Login.Text;
                Password = PasswordBox_Password.Password;
                DBLogin = App.Settings.DBUser;
                DBPassword = App.Settings.DBPassword;

                //Connection to Database
                App.Settings.ConnectionString = App.CreateConnectionString(Host, DBLogin, Database, DBPassword);
                App.CreateModel();

                //Manipulations with the password
                string password = Password;

                //Finding user
                App.CurrentUser = App.Model.Users.First(user => user.Login == TextBox_Login.Text && user.Password == password);

                //Disconnecting
                App.DisposeModel();

                App.Settings.DBHost = Host;
                App.Settings.User = Login;
                if (App.Settings.Database != Database)
                    App.Settings.Database = Database;
                if ((CheckBox_Save.IsChecked == true) && password != null)
                    App.Settings.Password = password;

                App.Settings.Save();

                //Exiting this form

                this.DialogResult = true;
                this.Close();
            }
            catch (TimeoutException ex)
            {
                MessageBox.Show("Timeout error, check your internet connection settings", "Error occured!", MessageBoxButton.OK, MessageBoxImage.Hand);
                return;
            }
            catch
            {
                MessageBox.Show("");
            }
            finally
            {
                App.DisposeModel();
            }
        }
        private void Button_Exit_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
        private void Button_Register_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            Registration RegistrationForm = new Registration();
            RegistrationForm.ShowDialog();
            if (RegistrationForm.DialogResult.HasValue && RegistrationForm.DialogResult.Value)
            {
                this.ShowDialog();
            }
            else
            {
                this.ShowDialog();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TextBox_Host.Text = App.Settings.DBHost;
            TextBox_Database.Text = App.Settings.Database;

            TextBox_Login.Text = App.Settings.User;
            PasswordBox_Password.Password = App.Settings.Password;
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
