//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BaseManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Projects
    {
        public Projects()
        {
            this.Data = new HashSet<Data>();
            this.Users = new HashSet<Users>();
        }
    
        public string Name { get; set; }
    
        public virtual ICollection<Data> Data { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
