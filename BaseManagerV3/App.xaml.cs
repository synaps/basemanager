﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace BaseManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static BaseManager.Properties.Settings Settings = new Properties.Settings();
        public static Projects Project = new Projects();
        public static BaseManager.DatabaseModel Model;
        public static ObservableCollection<Users> Users = new ObservableCollection<Users>();
        public static ObservableCollection<Projects> Projects = new ObservableCollection<Projects>();

        //Current objects
        //public static BaseManagerLib.User CurrentUser;
        public static Users CurrentUser;
        public static Projects CurrentProject;

        public static string[] RemoteNetwork;

        public static string CreateConnectionString(string pHost, string pLogin, string pDatabase, string pPassword)
        {
            //initializing with metadata info
            string ConnectionString = @"metadata=res://*/Database.csdl|res://*/Database.ssdl|res://*/Database.msl;provider=MySql.Data.MySqlClient;";
            ConnectionString += string.Format("provider connection string=\"server={0};user id={1};database={2};pwd={3};persist security info=True\"", pHost, pLogin, pDatabase, pPassword);
            return ConnectionString;
        }
        public static void CreateModel()
        {
            Model = new DatabaseModel(Settings.ConnectionString);
        }
        public static void DisposeModel()
        {
            Model.Dispose();
        }
    }
}
