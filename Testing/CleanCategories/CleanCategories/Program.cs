﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;


namespace CleanCategories
{
    class Entry
    {
        public int Indent;
        public ArrayList List { get; set; }

        public Entry(int pIndent)
        {
            Indent = pIndent;
            List = new ArrayList();
        }
    }
    class Program
    {
        static string GetName(string pLine)
        {
            int Start = pLine.IndexOf(">", 5);
            int End = pLine.IndexOf("</");
            return pLine.Substring(Start + 1, End - Start -1);
        }

/*        static void Main(string[] args)
        {
            FileStream Cat = File.OpenRead(".\\Categories.xml");
            StreamReader SReader = new StreamReader(Cat);

            List<string> MainCategories = new List<string>();
            List<List<string>> SubCategories = new List<List<string>>();

            bool InMain = false, InSub = false;
            int MainC = 0, SubC = 0;
            int MainI = 0, SubI = 0;

            string Line = "";
            string NextLine = "";
            do
            {
                try
                {
                    //Reading
                    Line = NextLine;
                    NextLine = SReader.ReadLine();
                    if (NextLine == null)
                        break;

                    //Trimming
                    Line = Line.Trim();
                    NextLine = NextLine.Trim();

                    if ((Line != null && Line.StartsWith("<li>")) && (NextLine != null && NextLine.StartsWith("<ul>")))
                    {
                        MainCategories.Add(Line);
                        InMain = true; MainC++; MainI++;
                        Console.WriteLine("+{3} MainCategory ({2}) added. \t\tMain={0}, Sub={1}", MainC, SubC, GetName(Line), MainI);
                    }
                    if((Line != null && Line.StartsWith("<ul>")) && (NextLine != null && NextLine.StartsWith("<li>")))
                    {
                        SubCategories.Add(new List<string>());
                        InSub = true; SubC++; SubI++;
                        Console.WriteLine("++{2} Empty SubCategory added. \tMain={0}, Sub={1}", MainC, SubC, SubI); 
                    }
                    if (InSub && !Line.StartsWith("<ul>"))
                    {
                        SubCategories[SubCategories.Count - 1].Add(Line);
                        Console.WriteLine("+++ Entry ({0}) added to Subcategory", GetName(Line));
                    }
                    if (Line != null && Line.StartsWith("</ul>"))
                    {
                        InSub = false; SubC--;
                        Console.WriteLine("-- SubCategory ends. \t\tMain={0}, Sub={1}", MainC, SubC);
                    }
                    if(Line != null && Line.StartsWith("</li>"))
                    {
                        InMain = false; MainC--;
                        Console.WriteLine("- MainCategory ends. \t\tMain={0}, Sub={1}", MainC, SubC);
                    }

             
                }
                catch
                {
                    Console.WriteLine("!** Error ***");
                    Console.ReadLine();
                }
            } while (Line != null && NextLine != null);
            Console.WriteLine("That's it \n\n\n\n");

            FileStream FStream = new FileStream("CleanCategories.txt", FileMode.Create);
            StreamWriter SWrite = new StreamWriter(FStream);
            for (int i = 0; i < MainCategories.Count; i++)
            {
                SWrite.WriteLine(GetName(MainCategories[i]));
                foreach (var item in SubCategories[i])
                {
                    SWrite.WriteLine("-- {0}", GetName(item));
                }
            }
            Console.ReadLine();
        }
 */
        static void Main()
        {
            FileStream Cat = File.OpenRead(".\\Categories.xml");
            StreamReader SReader = new StreamReader(Cat);

            int IndentLevel = 0;
            string Line = "", NextLine = "";
            bool InMain, InSub;
            List<Entry> Collections = new List<Entry>();

            do
            {
                Line = NextLine;
                NextLine = SReader.ReadLine();
                if (String.IsNullOrEmpty(NextLine))
                    break;
                NextLine = NextLine.Trim();

                if (Line.StartsWith("<li>") && NextLine.StartsWith("<ul>"))
                {

                    Collections.Add(new Entry(IndentLevel));
                    Collections[Collections.Count - 1].List.Add(GetName(Line));
                    Console.WriteLine("+ lvl={0} Name={1}", IndentLevel, GetName(Line));
                    IndentLevel++;
                }
                if (Line.StartsWith("<ul>") && NextLine.StartsWith("<li>"))
                {
                    //IndentLevel++;
                    Collections.Add(new Entry(IndentLevel));
                    Console.WriteLine(IndentLevel);
                }
                if (Line.StartsWith("</ul>"))
                {
                    //IndentLevel--;
                    Console.WriteLine(IndentLevel);
                }
                if (Line.StartsWith("</li>"))
                {
                    IndentLevel--;
                    Console.WriteLine(IndentLevel);

                }
                if (Line.StartsWith("<li>") && (NextLine.StartsWith("<li>") || NextLine.StartsWith("</ul>")))
                {
                    Collections[Collections.Count - 1].List.Add(GetName(Line));
                    Console.WriteLine("++ SubLvl={0} Name={1}", IndentLevel, GetName(Line));
                }
            } while (true);

            Console.WriteLine("That's it \n\n\n\n");

            FileStream FStream = new FileStream("CleanCategories.txt", FileMode.Create);
            StreamWriter SWrite = new StreamWriter(FStream);

            foreach (var item in Collections)
            {
                string indent = "";
                for (int i = 0; i < item.Indent; i++)
                {
                    indent += "-->";
                }

                foreach (var itema in item.List)
                {
                    Console.WriteLine(itema);
                    SWrite.WriteLine("{0} {1}", indent, itema);
                }
            }

            SReader.Close();
            Cat.Close();
            SWrite.Close();
            FStream.Close();
            Console.ReadLine();
        }
    }
}
