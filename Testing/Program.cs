﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Security;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Collections.ObjectModel;

using BaseManagerLib;

namespace Testing
{
    class Program
    {

        static Random rnd = new Random();
        static void Main(string[] args)
        {
            BaseManagerLib.Invoice.CreateInvoice(Environment.CurrentDirectory + "/Header.png", Environment.CurrentDirectory + "/Footer.png", "invoice1", new List<string>(){"0","1","2"});
            System.Diagnostics.Process.Start(Environment.CurrentDirectory + "/invoices/Invoice1.pdf");

            return;

            Console.Write("Host: ");
            string Host = Console.ReadLine();
            Console.Write("Database: ");
            string Database = Console.ReadLine();
            Console.Write("User: ");
            string User = Console.ReadLine();
            Console.Write("Password: ");
            string Password = Console.ReadLine();
            using(var Model = new InfolehtEntities(CreateConnectionString(Host, User, Database, Password)))
            {
                foreach (var item in Model.Data)
                {
                    Console.WriteLine(item.Firm);
                }
            }
            Console.ReadLine();
            return;
            using (var contex = new InfolehtEntities(CreateConnectionString(Host, User, Database, Password)))
            {

                try
                {
                    var Data = contex.Data;
                    //ObservableCollection<Testing.Data> Col = new ObservableCollection<Data>(Data.ToList());
                    //var List = Data.ToList();
                    //int i = 0;
                    //while (i < 5)
                    //{
                    //    List.Add(new Data() { Id = 0, Project = "basemanager" });
                    //    i++;
                    //}
                    Data.Add(new Testing.Data() { Id = 0, Project = "basemanager", Firm = "Firm", ContactPerson = "pers", ContactLanguage = "Russian"});
                    contex.SaveChanges();


                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }

            return;
            try
            {
                using (var contex = new InfolehtEntities(CreateConnectionString(Host, User, Database, Password)))
                {
                    var Users1 = contex.Users.Where(c => c.IdCode.StartsWith("00")).ToList();
                    foreach (var user in Users1)
                    {
                        Console.WriteLine("{0} {1}. Login property: {2} {3}. Priveleges: {4}", user.FirstName, user.LastName, user.Login, user.IdCode, user.Priveleges);
                    }
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            Console.WriteLine("----------------------------");
            Console.Write("Press <Enter> to terminate the program");
            Console.ReadLine();
        }
        static int NextDigit()
        {

            int rnd1 = rnd.Next(0, 9);
            return rnd1;
        }
        public static string CreateConnectionString(string pHost, string pLogin, string pDatabase, string pPassword)
        {
            //initializing with metadata info
            string ConnectionString = @"metadata=res://*/Data.csdl|res://*/Data.ssdl|res://*/Data.msl;provider=MySql.Data.MySqlClient;";
            ConnectionString += string.Format("provider connection string=\"server={0};user id={1};database={2};password={3};persist security info=True\"", pHost, pLogin, pDatabase, pPassword);
            return ConnectionString;
        }
    }
}
