CREATE TABLE Users
( IdCode varchar(11) not null,
FirstName varchar(20),
LastName varchar(20),
Login varchar(20) not null,
Password varchar(20) not null,
Priveleges tinyint not null,
CONSTRAINT pk_users PRIMARY KEY (IdCode))ENGINE=INNODB;

CREATE TABLE Projects
( Name varchar(20) not null,
CONSTRAINT pk_projects PRIMARY KEY (Name))ENGINE=INNODB;

CREATE TABLE ProjectUsers
( Project varchar(20) not null,
User varchar(11) not null,
CONSTRAINT pk_projectusers PRIMARY KEY (Project, User),
CONSTRAINT fk_users_pusers FOREIGN KEY (User) REFERENCES Users(IdCode),
CONSTRAINT fk_projects_pusers FOREIGN KEY (Project) REFERENCES Projects(Name))ENGINE=INNODB;

CREATE TABLE Diapasons
( Project varchar(20) not null,
Start int not null,
Stop int not null,
CONSTRAINT pk_diapasons PRIMARY KEY (Project, Start, Stop),
CONSTRAINT fk_projects_diapasons FOREIGN KEY (Project) REFERENCES Projects(Name))ENGINE=INNODB;

CREATE TABLE Data
( Id int not null auto_increment,
Firm varchar(30) not null,
PhysicalAddress varchar(30),
WebAddress varchar(30),
ContactPerson varchar(20) not null,
ContactPhone1 varchar(8),
ContactPhone2 varchar(8),
ContactEmail varchar(20),
LastContact Date,
NextContact Date,
Category varchar(20),
Notes text,
Project varchar(20) not null,
CONSTRAINT pk_data PRIMARY KEY (Id),
CONSTRAINT fk_projects_data FOREIGN KEY (Project) REFERENCES Projects(Name))ENGINE=INNODB;