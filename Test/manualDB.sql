#Current database describes FIRMS
#Queries should be simple
#Are the tables keep simple atomic info?

USE sands;

DROP TABLE test_table;

CREATE TABLE test_table
( 
ID int AUTO_INCREMENT,
firm_name varchar(40) not null,
when_published date,
physical_address varchar(40),
site varchar(30),
speaking_language varchar(10) not null,
contact_persone varchar(12) not null,
contact_phone1 varchar(8) not null,
contact_phone2 varchar(8),
contact_email varchar(40),
email_sent bool default false,
email_sent_date date,
next_contact date, # maybe better to use @datetime instead
activity varchar(30) not null,
notes blob,
PRIMARY KEY(ID)
 );

INSERT INTO test_table
values (
null, 'Firm1', '2013-06-18', 'ehitajate tee 31-2', 'http://sands.org', 'russian', 'Rustam', '53243334', null, 'rrahimgulov772@gmail.com', true, '2013-06-19', null, 'creating DB', "12 grade student"
);

ALTER TABLE test_table
ADD COLUMN was_contact bool not null default false
AFTER email_sent_date;

SELECT * FROM test_table;

UPDATE test_table
SET was_contact	= true
WHERE email_sent = true;

SELECT * FROM test_table
WHERE NOT email_sent = true;

DESC test_table;

SHOW CREATE TABLE test_table;
SHOW COLUMNS FROM test_table;
SHOW INDEX FROM test_table;

CREATE TABLE import_test
(
work_status varchar(5),
published varchar(5),
company_name varchar(15),
address varchar(20),
work_language varchar(10),
contact_persone varchar(15),
telephone varchar(8),
email varchar(25),
email_status varchar(5),
email_sent date,
next_contact date,
activity varchar(20),
notes blob
);

SELECT * FROM import_test;

LOAD DATA LOCAL INFILE 'F:\ANDMEBAAS_example.csv' INTO TABLE import_test
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\n';

#--------------------------------------
#	Users
#--------------------------------------

CREATE TABLE users
(
id_user int not null auto_increment,
full_name varchar(50) not null,
nickname varchar(15) not null,
pwd varchar(15) not null,
id_code varchar(11) not null,
privelege_level int not null,
CONSTRAINT pk_users PRIMARY KEY (id_user)
);

INSERT INTO users
VALUES 
('Rustam Rahimgulov', 'synaps', 'russia13', '39508230816', 2),
('Testin Tester', 'test', 'test', '00000000000', 1);

SELECT * FROM users;

DESC users;

DROP TABLE users;

CREATE TABLE projects
(
project_name varchar(20) not null,
id_project int AUTO_INCREMENT not null,
id_user varchar(11),
CONSTRAINT pk_projects PRIMARY KEY (id_project),
CONSTRAINT fk_user FOREIGN KEY (id_user) REFERENCES users (id_code)
);
