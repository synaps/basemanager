﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BaseManager.Forms.ProjectManagerWindows
{
    /// <summary>
    /// Interaction logic for Projects.xaml
    /// </summary>
    public partial class Projects : Window
    {
        ObservableCollection<BaseManager.Projects> ProjectList;
        public Projects()
        {
            InitializeComponent();
        }

        //Projects
        private void Button_AddProject_Click(object sender, RoutedEventArgs e)
        {
            BaseManager.Projects ProjectToAdd = new BaseManager.Projects() { Name = TextBox_ProjectName.Text };

            DataTemplate dataTemplate = ListView_ProjectUsers.ItemTemplate;
            for (int i = 0; i < ListView_ProjectUsers.Items.Count; i++)
            {
                ListViewItem LVItem = ListView_ProjectUsers.ItemContainerGenerator.ContainerFromIndex(i) as ListViewItem;
                ContentPresenter templateParent = GetFrameworkElementByName<ContentPresenter>(LVItem);

                CheckBox CBox = dataTemplate.FindName("ListView_CheckBox", templateParent) as CheckBox;
                if((bool)CBox.IsChecked)
                    ProjectToAdd.Users.Add(ListView_ProjectUsers.Items[i] as BaseManager.Users);
            }

            App.Model.Projects.Add(ProjectToAdd);
            ProjectList.Add(App.Model.Projects.Find(TextBox_ProjectName.Text));
            App.Model.SaveChanges();
        }

        private void Button_DeleteProject_Click(object sender, RoutedEventArgs e)
        {
            string ProjectName = (ListView_Projects.SelectedItem as BaseManager.Projects).Name;
            ProjectList.Remove(ListView_Projects.SelectedItem as BaseManager.Projects);
            App.Model.Projects.Remove(App.Model.Projects.First(project => project.Name == ProjectName));
            App.Model.SaveChanges();
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void ListView_Projects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ListView_Projects.SelectedItem == null)
                    return;
                var Project = App.Model.Projects.FirstOrDefault(project => project.Name == ((BaseManager.Projects)ListView_Projects.SelectedItem).Name);
                var ProjectUsers = Project.Users.ToList();

                DataTemplate dataTemplate = ListView_ProjectUsers.ItemTemplate;

                //Clear before use
                for (int i = 0; i < ListView_ProjectUsers.Items.Count; i++)
                {
                    ListViewItem LVItem = ListView_ProjectUsers.ItemContainerGenerator.ContainerFromIndex(i) as ListViewItem;
                    ContentPresenter templateParent = GetFrameworkElementByName<ContentPresenter>(LVItem);

                    CheckBox CBox = dataTemplate.FindName("ListView_CheckBox", templateParent) as CheckBox;
                    CBox.IsChecked = false;
                }

                //Set proper check boxes to @true
                foreach (var item in ProjectUsers)
                {
                    if (ListView_ProjectUsers.Items.Contains(item))
                    {
                        int index = ListView_ProjectUsers.Items.IndexOf(item);
                        ListViewItem LVItem = ListView_ProjectUsers.ItemContainerGenerator.ContainerFromIndex(index) as ListViewItem;
                        ContentPresenter templateParent = GetFrameworkElementByName<ContentPresenter>(LVItem);

                        CheckBox CBox = dataTemplate.FindName("ListView_CheckBox", templateParent) as CheckBox;
                        CBox.IsChecked = true;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Closing connection
            App.DisposeModel();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //Connection
                App.CreateModel();

                var projects = App.Model.Projects.ToList();
                ProjectList = new ObservableCollection<BaseManager.Projects>(projects);
                ListView_Projects.ItemsSource = ProjectList;

                var Users = App.Model.Users.ToList();
                ListView_ProjectUsers.ItemsSource = Users;

            }
            catch
            {
                MessageBox.Show("");
            }
        }

        private static T GetFrameworkElementByName<T>(FrameworkElement referenceElement) where T : FrameworkElement
        {
            FrameworkElement child = null;
            for (Int32 i = 0; i < VisualTreeHelper.GetChildrenCount(referenceElement); i++)
            {
                child = VisualTreeHelper.GetChild(referenceElement, i) as FrameworkElement;
                System.Diagnostics.Debug.WriteLine(child);
                if (child != null && child.GetType() == typeof(T))
                { break; }
                else if (child != null)
                {
                    child = GetFrameworkElementByName<T>(child);
                    if (child != null && child.GetType() == typeof(T))
                    {
                        break;
                    }
                }
            }
            return child as T;
        }
    }
}
