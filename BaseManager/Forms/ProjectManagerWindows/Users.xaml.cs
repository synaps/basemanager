﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace BaseManager.Forms.ProjectManagerWindows
{
    /// <summary>
    /// Interaction logic for Users.xaml
    /// </summary>
    public partial class Users : Window
    {
        //Variables
        bool UserChanged = false;


        public Users()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //Connection
                App.CreateModel();

                var Users = App.Model.Users.ToList();
                DataGrid_Users.ItemsSource = Users;
            }
            catch
            {
                MessageBox.Show("");
            }
        }

        private void Button_AddUser_Click(object sender, RoutedEventArgs e)
        {
            //switch (ComboBox_Permissions.Text)
            //{
            //    case "Grand Administrator":
            //        ComboBox_Permissions.Tag = (sbyte)3;
            //        break;
            //    case "Administrator":
            //        ComboBox_Permissions.Tag = (sbyte)2;
            //        break;
            //    case "User":
            //        ComboBox_Permissions.Tag = (sbyte)1;
            //        break;
            //}
            //BaseManager.Users User = new BaseManager.Users() { FirstName = TextBox_FirstName.Text, LastName = TextBox_LastName.Text, IdCode = TextBox_IDCode.Text, Login = TextBox_Nickname.Text, Password = TextBox_Password.Text, Privileges = (sbyte)ComboBox_Permissions.Tag };
            //App.Users.Add(User);
            //App.Model.Users.Add(User);
        }
        private void Button_RemoveUser_Click(object sender, RoutedEventArgs e)
        {
            //App.Users.RemoveUser(ListBox_Users.SelectedItem as BaseManagerLib.User);
        }

        private void Button_Ok_Click(object sender, RoutedEventArgs e)
        {
            App.Model.SaveChanges();
            App.Model.Dispose();

            this.DialogResult = true;
            this.Close();
        }

        private void Label_Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Disconnection
            App.Model.SaveChanges();
            App.DisposeModel();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            UserChanged = true;
        }
        private void DataGrid_Users_SelectionChanged(object sender, EventArgs e)
        {
            if (UserChanged)
            {
                UserChanged = false;
                BaseManager.Users LocalUser = DataGrid_Users.SelectedItem as BaseManager.Users;
                BaseManager.Users UserInDB = App.Model.Users.Find(LocalUser.IdCode);

                if (UserInDB.FirstName != LocalUser.FirstName)
                    UserInDB.FirstName = LocalUser.FirstName;
                if (UserInDB.LastName != LocalUser.LastName)
                    UserInDB.LastName = LocalUser.LastName;
                if (UserInDB.Login != LocalUser.Login)
                    UserInDB.Login = LocalUser.Login;
                if (UserInDB.Password != LocalUser.Password)
                    UserInDB.Password = LocalUser.Password;
            }
        }
    }
}
